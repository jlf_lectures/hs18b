class Stack[A] {
  //Retourne vrai si la pile est vide
  def isEmpty: Boolean
  // Retourne le nombre d'éléments de la pile
  def size: Int
  // Empile un élément sur la pile
  def push( a: A ): Unit
  // Dépile et retourne le sommet de la pile
  def pop: A
  //Inverse les deux éléments au sommet de la pile
  //ne fait rien si moins de deux éléments
  def swap: Unit
}

