package ch.unige.hepia.tp

case class Fraction( num: Int, den: Int ) {

  def simplify: Fraction = {
    val pgcd = Euclid.gcd( num, den )
    Fraction( num/pgcd, den/pgcd )
  }
  def inverse: Fraction = Fraction(den,num)

  def +( that: Fraction ) =
    Fraction(
      this.num*that.den + that.num*this.den,
      this.den*that.den
    ).simplify

  def -( that: Fraction ) =
    this + Fraction( -that.num, that.den )

  def *(that: Fraction ) =
    Fraction(
      this.num * that.num,
      this.den * that.den
    ).simplify

  def /(that: Fraction ) = this * that.inverse

  override def hashCode: Int = {
    val s = simplify
    s.num ^ s.den
  }

  override def equals( that: Any ) = that match {
    case f:Fraction => {
      val f2 = f.simplify
      val self = this.simplify
      f2.num == self.num && f2.den == self.den
    }
    case _ => false
  }


}
