package ch.unige.hepia.tp

object Euclid {

  def gcd( i: Int, j: Int ): Int = {
    var a = i.max(j)
    var b = i.min(j)
    var r = a % b
    while ( r != 0 ) {
      a = b
      b = r
      r = a % b
    }
    b
  }

  // gcd de maniere recursive

  def gcd2(  i: Int, j: Int ): Int = {
    def gcdRec( i: Int, j: Int ): Int = {
      if ( j == 0 )  i
      else gcdRec ( j , i % j )
    }
    if( i < j ) gcdRec(j,i)
    else gcdRec(i,j)
  }

  def main( args: Array[String] ): Unit = {

    println( gcd( 100, 30 ) + " recursive : " + gcd2( 100, 30 ) )  //=> 10
    println( gcd( 30, 100 ) + " recursive : " + gcd2( 30, 100 ) )  //=> 10
    println( gcd( 12, 3732 ) + " recursive : " + gcd2( 12, 3732 ) ) //=> 12
    println( gcd( 1, 3732 ) + " recursive : " + gcd2( 1, 3732 ) )  //=> 1
    println( gcd( 25, 3732 ) + " recursive : " + gcd2( 25, 3732) ) //)> 1
  }
}
