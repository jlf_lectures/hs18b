package exo

import java.util.ArrayList

class Stack[A] {

  private val lst = new ArrayList[A]()

  def isEmpty: Boolean = lst.isEmpty
  def size: Int = lst.size

  def push( a: A ): Unit = {
    lst.add(a)
    ()
  }

  def pop(): A = lst.remove( size - 1 )

  def swap(): Unit = {
    if( size >= 2 ) {
      val a = pop()
      val b = pop()
      push(a)
      push(b)
    }
  }


}
