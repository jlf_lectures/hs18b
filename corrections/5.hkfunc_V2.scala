package ch.hepia.tpscala

/* Implémentez les fonctions suivantes. Vous ne pouvez utiliser que les
 *  méthode de 'List' vues dans les exercices précédents.
 */
object HKFunc {

  /* La fonction 'map' applique une fonction 'f' sur chaque élément de
   * la liste 'as'.  La liste résultat doit avoir la même longueur que
   * l'argument.
   */
  def mapRev[A,B]( as: List[A] )( f: A=>B ): List[B] = {
    def loop( rest: List[A], result: List[B] ): List[B] = rest match {
      case Nil => result
      case h :: t => loop( t, f(h) :: result )
    }
    loop(as, Nil)
  }

  def map[A,B]( as: List[A] )( f: A=>B ): List[B] =
    mapRev( mapRev(as)(f) )( identity )

  /* La fonction 'filter' utilise le prédicat 'f' pour déterminer quel
   * élément garder. Le résultat peut être vide, mais l'ordre doit
   * être préservé.
   */
  def filter[A]( as: List[A] )( f: A=>Boolean ): List[A] = {
    def loop( rest: List[A], result: List[A] ): List[A] = rest match {
      case Nil => result
      case h :: t if f(h) => loop( t, h :: result )
      case _ :: t => loop( t, result )
    }
    loop(as, Nil).reverse
  }

  /* Réduit une liste 'as' en utilisant une opération binaire 'f'.  On
   * supposera que 'as' n'est pas vide.
   */
  def reduce[A]( as: List[A] )( f: (A,A)=>A ): A = {
    def loop( rest: List[A], result: A ): A = rest match {
      case Nil => result
      case h :: t => loop( t, f( result, h ) )
    }
    loop( as.tail, as.head )
  }

  def reduce1[A]( as: List[A] )( f: (A,A)=>A, e: A ): A = {
    def loop( rest: List[A], result: A ): A = rest match {
      case Nil => result
      case h :: t => loop( t, f( result, h ) )
    }
    loop( as, e )
  }

  def fold[A,B]( as: List[A] )( f: (B,A)=>B, e: B ): B = {
    def loop( rest: List[A], result: B ): B = rest match {
      case Nil => result
      case h :: t => loop( t, f( result, h ) )
    }
    loop( as, e )
  }

  /*fold[String,Int]( lines )( (sum,line) => sum + line.size, 0 )

  def reduce[A](  as: List[A] )( f: (A,A)=>A ): A = 
    fold[A,A]( as.tail )( f, as.head )*/

  def filterF[A]( as: List[A] )( f: A=>Boolean ): List[A] =
    fold[A,List[A]]( as ){
      (lst,a) => if( f(a) ) lst ++ List(a) else lst, //Attention O(n²)
      Nil
    }



  /* Transforme une fonction 'f' en une fonction s'appliquant sur une
  *  liste. Utiliser la fonction 'map' définie ci-dessus
  */
  def lift[A,B]( f: A=>B ): List[A]=>List[B] =
    lstA => map(lstA)(f)

  /* DIFFICILE. Transforme une liste 'as' au moyen de la fonction 'f'.
   * Cette fonction est appliquée à chaque élément de 'as' pour
   * produire une nouvelle liste (qui peut être vide). Le résultat est
   * la concaténation de chaque nouvelle liste en respectant l'ordre.
   */
  def bind[A,B]( as: List[A] )( f: A=>List[B] ): List[B] = {
    def loop( rest: List[A], result: List[B] ): List[B] = rest match {
      case Nil => result
      case h :: t => loop( t, result ++ f(h) )
    }
    loop(as, Nil)
  }
  def lift2[A,B,C]( as: List[A], bs: List[B] )( f: (A,B)=>C ): List[C] = 

}
