package ch.unige.hepia.tp

case class Fraction2 private( num: Int, den: Int ) {

  def inverse: Fraction2 = Fraction2(den,num)

  def +( that: Fraction2 ) =
    Fraction2.simplified(
      this.num*that.den + that.num*this.den,
      this.den*that.den
    )

  def -( that: Fraction2 ) =
    this + Fraction2( -that.num, that.den )

  def *(that: Fraction2 ) =
    Fraction2.simplified(
      this.num * that.num,
      this.den * that.den
    )

  def /(that: Fraction2 ) = this * that.inverse

  override def toString: String = s"$num\\$den"

}

object Fraction2 {

  def simplified( num: Int, den: Int ) = {
    val gcd = Euclid.gcd(num, den)
    Fraction2(num/gcd,den/gcd)
  }

  implicit class Int2Fraction( self: Int ) {
    def \( that: Int ) = Fraction2.simplified(self,that)
  }

}
